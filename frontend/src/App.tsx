import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Header from './components/Header/Header';
import './styles/App.css';
import PostList from './components/Post/PostList';
import Login from './components/Auth/Login';
import AddPost from './components/Post/AddPost';
import Registration from './components/Auth/Registration';
import { useTypesSelector } from './store/hooks/user.type.selector';
import { useDispatch } from 'react-redux';
import { Dispatch, useEffect } from 'react';
import { UserAction, UserActionTypes } from './store/reducers/user.reducer';
import { fetchUser } from './store/hooks/fetch.user';
import PostEdit from './components/Post/EditPost';



function App() {

  const state = useTypesSelector(state=>state.user);
  const dispatch:any = useDispatch();
  useEffect(()=>{
    const token = localStorage.getItem('token');
    if(token){
      dispatch(fetchUser(token))
    }
  },[]);

  return (
    <div>

      <Router>
        <Header/>
        <Routes>
          <Route path="/" element={<PostList />} />
          <Route path="/login" element={<Login />} />
          <Route path='/post-update/:id' element={<PostEdit/>}/>
          <Route path="/registration" element={<Registration />} />
          <Route path="/post-create" element={<AddPost />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
