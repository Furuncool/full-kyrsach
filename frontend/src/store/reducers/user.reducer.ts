
interface Role{
  system_name:string
}

interface User{
  id:number,
  name:string,
  email:string,
  token:string,
  role:Role
}

interface userState{
  user:User
}

const initialState:userState = {
  user:<User>{},
}


export enum UserActionTypes{
  ADD_USER = "ADD_USER",
}

interface AddUser{
  type:UserActionTypes.ADD_USER,
  payload:any
} 


export type UserAction = AddUser;

export const userReducer = (state = initialState,action:UserAction):userState =>{
    switch(action.type){
        case UserActionTypes.ADD_USER:
          return {user:action.payload}

        default:
          return state;

    }
}