import redact from '../../static/img/png-clipart-computer-icons-drawing-editing-pencil-pencil-angle-pencil.png'
import del from '../../static/img/scale_1200.webp'
import React from "react";
import { Link } from 'react-router-dom';
import { useTypesSelector } from '../../store/hooks/user.type.selector';
import PostService from '../../service/PostService';

export interface User {
  id:number,
  name: string,
  email: string,
}

export interface post {
  id:number,
  title: string,
  content: string,
  author: User,
  time: string,
  created_at: string,
}

export interface props {
  post: post,
  deletePost:Function,
}

function PostItem(props: props) {
  const state = useTypesSelector(state=>state.user);
  const postService = new PostService();
  const deletePost = ()=>{
      props.deletePost(props.post.id);
      postService.delete(props.post.id);
  }
  return (
    <div className="post">
      <div className="post__head">
        <h2 className='post_title'>{props.post.title}</h2>
        {Object.keys(state.user).length>0&&(state.user.role.system_name == 'admin'||state.user.id==props.post.author.id)  ?(
        <div className="post__buttons">
          <Link to={`/post-update/${props.post.id}`} className="post__button"><img src={redact} alt="" /></Link>
          <button onClick={deletePost} className="post__button"><img src={del} alt="" /></button>
        </div>
        ):null
        }
      </div>
      <div className="post_body">{props.post.content}</div>
      <div className="post__footer">
        <div className="post_user">{props.post.author.name}</div>
        <div className="post_time">{new Date(props.post.created_at).toLocaleDateString()}</div>
      </div>
    </div>
  );
};


export default PostItem;