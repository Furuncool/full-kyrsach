import React, { useEffect, useState } from "react";
import PostItem, { post } from "./PostItem";
import PostService from "../../service/PostService";


const PostList = () => {
  const postService = new PostService();
  const [data,setData] = useState([]);

  useEffect(() => {
    const getPost =async()=>{
      const fetchData = await postService.getPosts();
      setData(fetchData.data);
    };
    getPost();
    
  },[]);
  const deletePost = (id:number) =>{
    const index = data.findIndex((el:post)=>el.id == id);
    setData(data.filter((el:post)=>{
      if(el.id!=id){
        return el
      }
    }));
  }

  return (
    <div className="postList">
      {data.map((post:any) => 
        <PostItem post={post} deletePost={deletePost} key={post.id}/>
      )}
    </div>
  );
};

export default PostList;