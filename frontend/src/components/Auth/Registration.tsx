
import React, { FormEvent, useState } from "react";
import { useNavigate } from "react-router";
import axios from '../../index';
import { Link } from "react-router-dom";


const Registration = () => {
  const navigate = useNavigate();
  const [form, setForm] = useState({ email: '',name:'', password: '' });

  async function fetchForm(e: FormEvent) {
    e.preventDefault();
    await axios.post('/register',form);
    navigate('/login');
  }
  return (
    <form className="auth__form" onSubmit={fetchForm}>
      <div className="auth__input-block">
        <label htmlFor="login">Логин</label>
        <input
          value={form.email}
          onChange={e => setForm({ ...form, email: e.target.value })}
          id="login"
          type="text"
          placeholder="Логин"
          required
        />
      </div>
      <div className="auth__input-block">
        <label htmlFor="name">Имя</label>
        <input
          value={form.name}
          onChange={e => setForm({ ...form, name: e.target.value })}
          id="name"
          type="text"
          placeholder="Имя"
          required
        />
      </div>
      <div className="auth__input-block">
        <label htmlFor="pass">Пароль</label>
        <input
          value={form.password}
          onChange={e => setForm({ ...form, password: e.target.value })}
          id="pass"
          type="password"
          placeholder="Пароль"
          required
        />
      </div>
      <div className="auth__subform-block">

        <button>Зарегистрироваться</button>
        <Link to='/login'>Войти</Link>
      </div>
    </form>
  );
};

export default Registration;