import axios from "../index";
export default class PostService{
  async getPosts (){
    return await axios.get('/post');
  }

  async getPost(id:string|undefined){
    if(id){
      return await axios.get(`/post/${id}`);
    }
    return null;

  }

  async update(id:number|undefined,data:Object){
    if(id){
      return await axios.post(`/post/${id}`,data);
    }
    return null;
  }

  async delete(id:number){
    return await axios.delete(`/post/${id}`)
  }
}; 